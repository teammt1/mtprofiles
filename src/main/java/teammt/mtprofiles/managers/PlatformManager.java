package teammt.mtprofiles.managers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.configuration.ConfigurationSection;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.mtprofiles.data.Platform;

public class PlatformManager extends Registerable {

    public PlatformManager(MLib lib) {
        super(lib);
    }

    public Set<String> getPlatformNames() {
        return lib.getConfigurationAPI().getConfig().getConfigurationSection("platforms").getKeys(false);
    }

    private Map<String, Platform> platforms = new HashMap<>();

    public Map<String, Platform> getPlatforms() {
        if (platforms.size() > 0)
            return platforms;

        for (String cr : getPlatformNames()) {
            ConfigurationSection section = lib.getConfigurationAPI().getConfig()
                    .getConfigurationSection("platforms." + cr);

            Platform platform = new Platform(section);
            platforms.put(platform.getName().toLowerCase(), platform);
        }

        return platforms;
    }

    public Platform getPlatform(String platform) {
        return getPlatforms().get(platform.toLowerCase());
    }
}