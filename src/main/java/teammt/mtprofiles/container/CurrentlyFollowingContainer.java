package teammt.mtprofiles.container;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import masecla.mlib.classes.Replaceable;
import masecla.mlib.containers.instances.SquaredPagedContainer;
import masecla.mlib.main.MLib;
import teammt.mtprofiles.data.PlayerProfile;
import teammt.mtprofiles.managers.PlayerProfileManager;

public class CurrentlyFollowingContainer extends SquaredPagedContainer {

    private PlayerProfileManager profileManager;
    private SocialMediaContainer mediaContainer;

    public CurrentlyFollowingContainer(MLib lib, PlayerProfileManager profileManager,
            SocialMediaContainer mediaContainer) {
        super(lib);
        this.profileManager = profileManager;
        this.mediaContainer = mediaContainer;
    }

    @Override
    public void usableClick(InventoryClickEvent event) {
        event.setCancelled(true);

        if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR)
            return;
        String tag = event.getCurrentItem().getItemMeta().getDisplayName().substring(2);
        if (tag == null)
            return;
        for (OfflinePlayer op : Bukkit.getOfflinePlayers()) {
            if (op.getName().equals(tag)) {
                lib.getContainerAPI().openFor((Player) event.getWhoClicked(), SocialMediaContainer.class);
                return;
            }
        }
    }

    @Override
    public List<ItemStack> getOrderableItems(Player player) {
        List<ItemStack> result = new ArrayList<>();

        UUID targetUuid = this.mediaContainer.getLookingAt().get(player.getUniqueId());
        PlayerProfile targetProfile = profileManager.getPlayerProfile(targetUuid);

        for (UUID uuid : targetProfile.getFollowing()) {
            PlayerProfile currentProfile = profileManager.getPlayerProfile(uuid);
            OfflinePlayer op = Bukkit.getOfflinePlayer(uuid);

            ItemStack skull = currentProfile.getSkull(lib);
            ItemMeta skullMeta = skull.getItemMeta();

            skullMeta.setDisplayName(lib.getMessagesAPI().getPluginMessage("following-container-skull-name", player,
                    new Replaceable("%target%", op.getName())));
            skullMeta.setLore(lib.getMessagesAPI().getPluginListMessage("following-container-skull-lore", player,
                    new Replaceable("%target%", op.getName())));
            skull.setItemMeta(skullMeta);

            result.add(skull);
        }

        return result;
    }

    @Override
    public int getSize(Player player) {
        return 27;
    }

    @Override
    public int getUpdatingInterval() {
        return 10;
    }

    @Override
    public String getTitle(Player player) {
        UUID targetUuid = this.mediaContainer.getLookingAt().get(player.getUniqueId());
        OfflinePlayer target = Bukkit.getOfflinePlayer(targetUuid);

        return lib.getMessagesAPI().getPluginMessage("following-container-title", player,
                new Replaceable("%target%", target.getName()));
    }

}
