package teammt.mtprofiles.container;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.Getter;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.classes.builders.InventoryBuilder;
import masecla.mlib.classes.builders.ItemBuilder;
import masecla.mlib.containers.generic.ImmutableContainer;
import masecla.mlib.main.MLib;
import teammt.mtprofiles.data.Platform;
import teammt.mtprofiles.data.PlayerProfile;
import teammt.mtprofiles.managers.PlatformManager;
import teammt.mtprofiles.managers.PlayerProfileManager;

public class SocialMediaContainer extends ImmutableContainer {

    private PlatformManager platformManager;
    private PlayerProfileManager profileManager;

    @Getter
    private Map<UUID, UUID> lookingAt = new HashMap<>();

    public SocialMediaContainer(MLib lib, PlatformManager platformManager, PlayerProfileManager profileManager) {
        super(lib);
        this.platformManager = platformManager;
        this.profileManager = profileManager;
    }

    @Override
    public void onTopClick(InventoryClickEvent event) {
        event.setCancelled(true);

        Player player = (Player) event.getWhoClicked();
        if (event.getSlot() == 31) {
            player.closeInventory();
            return;
        }

        if (event.getSlot() == 35) {
            lib.getContainerAPI().openFor(player, CurrentlyFollowingContainer.class);
            return;
        }
    }

    @Override
    public int getSize(Player player) {
        return 36;
    }

    @Override
    public int getUpdatingInterval() {
        return 10;
    }

    @Override
    public boolean requiresUpdating() {
        return true;
    }

    @Override
    public Inventory getInventory(Player player) {
        UUID target = lookingAt.get(player.getUniqueId());
        PlayerProfile targetProfile = profileManager.getPlayerProfile(target);

        InventoryBuilder inventory = new InventoryBuilder()
                .size(getSize(player)).messages()
                .title("platforms-container-title")
                .border(getSocialMediaContainerBorder())
                .setItem(4, getPlayerSkull(targetProfile))
                .setItem(35, getFollowingContainer(targetProfile))
                .setItem(31, getSocialMediaContainerClose())
                .setItem(0, getTimes(targetProfile))
                .replaceable("%target%", targetProfile.getName());

        inventory = getSocialMediaContainerPlatforms(inventory, targetProfile);

        return inventory.build(lib, player);
    }

    private InventoryBuilder getSocialMediaContainerPlatforms(InventoryBuilder result, PlayerProfile targetProfile) {
        int[] availableSlots = { 10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25 };
        int maxSlots = Math.min(availableSlots.length, platformManager.getPlatforms().size());

        if (platformManager.getPlatforms().size() > availableSlots.length) {
            OfflinePlayer op = Bukkit.getOfflinePlayer(targetProfile.getUuid());
            lib.getLoggerAPI().warn("Player " + op.getName() + " has more social medias than allowed!");
        }

        // TODO: Give admins the option to specify the index themselves.
        // don't force em into this bs
        List<Platform> platforms = platformManager.getPlatforms().values()
                .stream().sorted(Comparator.comparing(Platform::getName)).collect(Collectors.toList());

        for (int i = 0; i < maxSlots; i++) {
            Platform platform = platforms.get(i);
            ItemBuilder platformItem = buildPlatformItemstack(platform);

            String platformTag = "N/A";
            if (targetProfile.getPlatformTags().containsKey(platform))
                platformTag = targetProfile.getPlatformTags().get(platform);

            platformItem.replaceable("%tag%", platformTag);

            result.setItem(availableSlots[i], platformItem);
        }

        return result;
    }

    private ItemBuilder buildPlatformItemstack(Platform platform) {
        return new ItemBuilder()
                .skull(platform.getSkull()).mnl("platforms-container-object")
                .replaceable("%color%", platform.getColor())
                .replaceable("%platform%", platform.getName());
    }

    private ItemStack getSocialMediaContainerBorder() {
        Material pane = null;
        if (lib.getCompatibilityApi().getServerVersion().getMajor() <= 12)
            pane = Material.matchMaterial("STAINED_GLASS_PANE");
        else
            pane = Material.matchMaterial("BLACK_STAINED_GLASS_PANE");
        ItemBuilder paneItem = new ItemBuilder(pane);
        if (lib.getCompatibilityApi().getServerVersion().getMajor() <= 12)
            paneItem = paneItem.data((byte) 15);
        return paneItem.name(" ").build(lib);
    }

    private ItemStack getSocialMediaContainerClose() {
        return new ItemBuilder(Material.BARRIER)
                .mnl("platforms-container-close")
                .build(lib);
    }

    private ItemStack getFollowingContainer(PlayerProfile targetProfile) {
        return new ItemBuilder(Material.FILLED_MAP).mnl("platforms-container-following")
                .replaceable("%target%", targetProfile.getName())
                .build(lib);
    }

    private ItemStack getPlayerSkull(PlayerProfile targetProfile) {
        ItemStack skull = targetProfile.getSkull(lib);
        ItemMeta meta = skull.getItemMeta();

        String status = targetProfile.getStatus();
        if (status == null || status.isEmpty())
            status = lib.getConfigurationAPI().getConfig().getString("default-status", "No Status!");

        Replaceable[] replaceables = {
                new Replaceable("%target%", targetProfile.getName()),
                new Replaceable("%status%", status)
        };

        meta.setDisplayName(lib.getMessagesAPI().getPluginMessage("platforms-container-skull-name", replaceables));
        meta.setLore(lib.getMessagesAPI().getPluginListMessage("platforms-container-skull-lore", replaceables));

        skull.setItemMeta(meta);
        return skull;
    }

    private ItemStack getTimes(PlayerProfile profile) {
        ItemBuilder item = new ItemBuilder(Material.CLOCK).mnl("platforms-container-times")
                .replaceable("%target%", profile.getName());

        DateFormat datetime = new SimpleDateFormat(lib.getConfigurationAPI().getConfig().getString("time-format"));
        datetime.setTimeZone(TimeZone.getTimeZone(lib.getConfigurationAPI().getConfig().getString("timezone")));

        String firstIn = "N/A";
        String lastIn = "N/A";
        String lastOut = "N/A";

        if (profile.getFirstLoggedIn() != 0)
            firstIn = datetime.format(profile.getFirstLoggedIn() * 1000);
        if (profile.getLastLoggedIn() != 0)
            lastIn = datetime.format(profile.getLastLoggedIn() * 1000);
        if (profile.getLastLoggedOut() != 0)
            lastOut = datetime.format(profile.getLastLoggedOut() * 1000);

        return item
                .replaceable("%first-in%", firstIn)
                .replaceable("%last-in%", lastIn)
                .replaceable("%last-out%", lastOut)
                .build(lib);
    }

    @EventHandler
    public void onInventoryClose(PlayerQuitEvent event) {
        lookingAt.remove(event.getPlayer().getUniqueId());
    }

}
