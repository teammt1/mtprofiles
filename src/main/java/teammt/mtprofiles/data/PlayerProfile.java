package teammt.mtprofiles.data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import masecla.mlib.main.MLib;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class PlayerProfile {
    @NonNull
    private UUID uuid;

    private String status = "";
    private long statusLastChanged = 0;

    private long firstLoggedIn = Instant.now().getEpochSecond();
    private long lastLoggedIn = 0;
    private long lastLoggedOut = 0;

    private List<UUID> following = new ArrayList<>();

    private Map<Platform, String> platformTags = new HashMap<>();

    public void addFollowing(UUID uuid) {
        following.add(uuid);
    }

    public void removeFollowing(UUID uuid) {
        following.remove(uuid);
    }

    public void removeStatus() {
        this.status = "";
    }

    @SuppressWarnings("deprecation")
    public ItemStack getSkull(MLib lib) {
        Player player = Bukkit.getPlayer(uuid);
        ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta skullMeta = (SkullMeta) playerHead.getItemMeta();

        if (lib.getCompatibilityApi().getServerVersion().getMajor() >= 13)
            skullMeta.setOwningPlayer(player);
        else
            skullMeta.setOwner(player.getName());

        playerHead.setItemMeta(skullMeta);
        return playerHead;
    }

    private transient OfflinePlayer offlinePlayer;

    public OfflinePlayer getOfflinePlayer() {
        if (offlinePlayer == null)
            offlinePlayer = Bukkit.getOfflinePlayer(uuid);
        return offlinePlayer;
    }

    public String getName() {
        return getOfflinePlayer().getName();
    }

}