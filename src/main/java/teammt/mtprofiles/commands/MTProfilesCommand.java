package teammt.mtprofiles.commands;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Registerable;
import masecla.mlib.classes.Replaceable;
import masecla.mlib.main.MLib;
import teammt.mtprofiles.container.SocialMediaContainer;
import teammt.mtprofiles.data.PlayerProfile;
import teammt.mtprofiles.managers.PlatformManager;
import teammt.mtprofiles.managers.PlayerProfileManager;

@RegisterableInfo(command = "mtprofiles")
public class MTProfilesCommand extends Registerable {

    private PlayerProfileManager profileManager;
    private PlatformManager platformManager;
    private SocialMediaContainer socialMediaContainer;

    public MTProfilesCommand(MLib lib, PlayerProfileManager playerProfileManager, PlatformManager platformManager,
            SocialMediaContainer socialMediaContainer) {
        super(lib);
        this.profileManager = playerProfileManager;
        this.platformManager = platformManager;
        this.socialMediaContainer = socialMediaContainer;
    }

    /**
     * Opens help menu
     */
    @SubcommandInfo(subcommand = "", permission = "teammt.mtprofiles.help")
    public void handleNoArgs(Player sender) {
        this.socialMediaContainer.getLookingAt().put(sender.getUniqueId(), sender.getUniqueId());
        lib.getContainerAPI().openFor(sender, SocialMediaContainer.class);
    }

    /**
     * Opens help menu
     */
    @SubcommandInfo(subcommand = "help", permission = "teammt.mtprofiles.help")
    public void handleHelp(Player sender) {
        lib.getMessagesAPI().sendMessage("help-message", sender);
    }

    /**
     * Reload configurations currently doing the config reload
     */
    @SubcommandInfo(subcommand = "reload", permission = "teammt.mtpolls.commands.reload")
    public void onReload(Player sender) {
        lib.getConfigurationAPI().reloadAll();
        lib.getMessagesAPI().reloadSharedConfig();
        this.platformManager.getPlatforms().clear();
        lib.getMessagesAPI().sendMessage("plugin-reloaded", sender);
    }

    /**
     * Opens the social media menu for the player
     */
    @SubcommandInfo(subcommand = "social", permission = "teammt.mtprofiles.social")
    public void handleSocial(Player sender) {
        this.socialMediaContainer.getLookingAt().put(sender.getUniqueId(), sender.getUniqueId());
        lib.getContainerAPI().openFor(sender, SocialMediaContainer.class);
    }

    /**
     * Opens the social media menu for other players
     */
    @SubcommandInfo(subcommand = "social", permission = "teammt.mtprofiles.social")
    public void handleSocial(Player sender, String targetName) {
        OfflinePlayer offlinePlayer = null;
        for (OfflinePlayer op : Bukkit.getOfflinePlayers()) {
            if (op.getName().equalsIgnoreCase(targetName))
                offlinePlayer = op;
        }

        if (offlinePlayer == null) {
            lib.getMessagesAPI().sendMessage("player-not-found", sender, new Replaceable("%player%", targetName));
            return;
        }

        UUID uuid = offlinePlayer.getUniqueId();
        if (!profileManager.hasProfile(uuid)) {
            lib.getMessagesAPI().sendMessage("no-social-profile", sender, new Replaceable("%player%", targetName));
            return;
        }

        this.socialMediaContainer.getLookingAt().put(sender.getUniqueId(), uuid);
        lib.getContainerAPI().openFor(sender, SocialMediaContainer.class);
    }

    @SubcommandInfo(subcommand = "platforms", permission = "teammt.mtprofiles.platforms")
    public void handlePlatforms(Player sender) {
        Set<String> platformList = this.platformManager.getPlatformNames();
        String platforms = String.join(", ", platformList);

        lib.getMessagesAPI().sendMessage("platform-list", sender, new Replaceable("%platforms%", platforms));
    }

    /**
     * Links a platform to the player
     */
    @SubcommandInfo(subcommand = "link", permission = "teammt.mtprofiles.link")
    public void handleLink(Player sender, String platform, String... tag) {
        profileManager.link(sender, platform.toLowerCase(), tag[0]);
    }

    /**
     * Unlinks a platform from the player
     */
    @SubcommandInfo(subcommand = "unlink", permission = "teammt.mtprofiles.unlink")
    public void unlink(Player sender, String platform) {
        profileManager.unlink(sender, platform.toLowerCase());
    }

    /**
     * Follows a person's socials
     */
    @SubcommandInfo(subcommand = "follow", permission = "teammt.mtprofiles.follow")
    public void handleFollow(Player sender, String target) {
        profileManager.follow(sender, target);
    }

    /**
     * Unfollows a person's socials
     */
    @SubcommandInfo(subcommand = "unfollow", permission = "teammt.mtprofiles.unfollow")
    public void handleUnfollow(Player sender, String target) {
        profileManager.unfollow(sender, target);
    }

    /**
     * Customizes message in socials GUI
     */
    @SubcommandInfo(subcommand = "status", permission = "teammt.mtprofiles.status")
    public void handleStatus(Player sender, String... statusList) {
        String status = String.join(" ", statusList);
        if (status.length() > 30) {
            lib.getMessagesAPI().sendMessage("message-too-long", sender);
            return;
        }

        PlayerProfile profile = profileManager.getPlayerProfile(sender.getUniqueId());

        long cooldown = lib.getConfigurationAPI().getConfig().getInt("status-cooldown");
        if (profile.getStatusLastChanged() + cooldown > Instant.now().getEpochSecond()) {
            long remainingTime = profile.getStatusLastChanged() + cooldown - Instant.now().getEpochSecond();
            lib.getMessagesAPI().sendMessage("status-cooldown", sender,
                    new Replaceable("%time%", lib.getTimesAPI().generateTime(remainingTime)));
            return;
        }

        profile.setStatus(status);
        profile.setStatusLastChanged(Instant.now().getEpochSecond());

        profileManager.saveProfile(profile);
        lib.getMessagesAPI().sendMessage("status-added", sender);
    }

    /**
     * Removes customized message in socials GUI
     */
    @SubcommandInfo(subcommand = "removestatus", permission = "teammt.mtprofiles.removestatus")
    public void handleRemoveStatus(Player sender) {
        PlayerProfile profile = profileManager.getPlayerProfile(sender.getUniqueId());
        if (profile.getStatus() == null || profile.getStatus().isEmpty()) {
            lib.getMessagesAPI().sendMessage("no-status", sender);
            return;
        }

        profile.removeStatus();
        profileManager.saveProfile(profile);
        lib.getMessagesAPI().sendMessage("status-removed", sender);
    }

    @Override
    public void fallbackCommand(CommandSender sender, String[] args) {
        if (args.length == 1) {
            if (sender instanceof Player)
                this.handleSocial((Player) sender, args[0]);
            else
                lib.getMessagesAPI().sendMessage("only-player", sender);
        } else {
            lib.getMessagesAPI().sendMessage("help-message", sender);
        }
    }
}
