package teammt.mtprofiles.main;

import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.mtprofiles.commands.MTProfilesCommand;
import teammt.mtprofiles.container.CurrentlyFollowingContainer;
import teammt.mtprofiles.container.SocialMediaContainer;
import teammt.mtprofiles.managers.PlatformManager;
import teammt.mtprofiles.managers.PlayerProfileManager;

public final class MTProfiles extends JavaPlugin {

    private MLib lib;
    private PlayerProfileManager playerProfileManager;
    private PlatformManager platformManager;

    @Override
    public void onEnable() {
        this.lib = new MLib(this);
        this.lib.getConfigurationAPI().requireAll();
        this.lib.getMessagesAPI().disableAntispam();

        // Managers
        this.platformManager = new PlatformManager(lib);
        this.playerProfileManager = new PlayerProfileManager(lib, platformManager);
        this.playerProfileManager.register();

        SocialMediaContainer mediaContainer = new SocialMediaContainer(lib, platformManager, playerProfileManager);
        mediaContainer.register();
        new CurrentlyFollowingContainer(lib, playerProfileManager, mediaContainer).register();

        // Commands
        new MTProfilesCommand(lib, playerProfileManager, platformManager, mediaContainer).register();
    }

}
